import React from 'react';
import {Image, Text, TouchableWithoutFeedback, View} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

class Staffers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            borderColor: '#b8569200'
        }
    }

    selectThis() {
        if(this.props.selectable) {
            let {selected, borderColor} = this.state;
            if(selected) {
                borderColor = '#b8569200';
            }
            else {
                borderColor = '#b85692';
            }
            selected = !selected;
            this.setState({
                selected, borderColor
            });
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.selectThis.bind(this)}>
            <View style={{marginRight: 20, padding: 10}}>
                <View style={{borderWidth: 10, borderRadius: 500, padding: 0, borderColor: this.state.borderColor}}>
                    <Image 
                        source={require('../../assets/profile-page/profile-picture.png')}
                        style={{width: wp('30%'), height: wp('30%')}}
                    />
                    {this.state.selected 
                    ?
                    <Image 
                        source={require('../../assets/misc/select-icon-pink.png')}
                        style={{width: wp('10%'), height: wp('10%'), position: 'absolute', right: 0, top: -15}}
                    />
                    :
                    <></>
                    }
                </View>
                <Text style={{alignSelf: 'center', color: '#ffffff', fontSize: wp('4%'), fontWeight: 'bold'}}>Robert John</Text>
                <Text style={{alignSelf: 'center', color: '#dabcee'}}>Barber</Text>
            </View>
            </TouchableWithoutFeedback>
        );
    }
}

export default Staffers;