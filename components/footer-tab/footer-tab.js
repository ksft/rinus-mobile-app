import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import NavigationService from '../../src/NavigationService';

// Non Active Icons
const nonActiveIcons = {
    home: require('../../assets/footer-tab/home_icon.png'),
    listing: require('../../assets/footer-tab/search_icon.png'),
    appointments: require('../../assets/footer-tab/calendar_icon.png'),
    profile: require('../../assets/footer-tab/user_icon.png'),
}

// Active Icons
const activeIcons = {
    home: require('../../assets/footer-tab/home_icon_active.png'),
    listing: require('../../assets/footer-tab/search_icon_active.png'),
    appointments: require('../../assets/footer-tab/calendar_icon_active.png'),
    profile: require('../../assets/footer-tab/user_icon_active.png'),
}

class FooterTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab : 1
        }
    }

    componentDidMount() {
        let {activeTab} = this.state;
        if(this.props.link) {
            activeTab = this.props.link;
            this.setState({
                activeTab
            });
        }
    }

    render() {
        return (
            <View style={styles.tabOuter}>
                <TouchableOpacity style={styles.tabButton}
                    onPress={() => {
                        NavigationService.navigate('Home');
                        this.setState({
                            activeTab: 1
                        });
                    }}
                >
                    <Image 
                        style={styles.tabButtonIcon} 
                        source={(this.state.activeTab === 1) ? activeIcons.home : nonActiveIcons.home} 
                    />
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabButton}
                    onPress={() => {
                        NavigationService.navigate('Listing');
                        this.setState({
                            activeTab: 2
                        });
                    }}
                >
                    <Image 
                        style={styles.tabButtonIcon} 
                        source={(this.state.activeTab === 2) ? activeIcons.listing : nonActiveIcons.listing} 
                    />
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabButton}
                    onPress={() => {
                        NavigationService.navigate('Appointments');
                        this.setState({
                            activeTab: 3
                        });
                    }}
                >
                    <Image 
                        style={styles.tabButtonIcon} 
                        source={(this.state.activeTab === 3) ? activeIcons.appointments : nonActiveIcons.appointments} 
                    />
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.tabButton}
                    onPress={() => {
                        NavigationService.navigate('Profile');
                        this.setState({
                            activeTab: 4
                        });
                    }}
                >
                    <Image 
                        style={styles.tabButtonIcon} 
                        source={(this.state.activeTab === 4) ? activeIcons.profile : nonActiveIcons.profile} 
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabOuter: {
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopLeftRadius: 17,
        borderTopRightRadius: 17,
        backgroundColor: '#ffffff',
        position: 'absolute',
		bottom: 0,
        left: 0,
        overflow: 'hidden',
        height: hp('8%'),
        width: wp('100%')
    },
    tabButton: {
        flex: 1,
        padding: hp('3%'),
        justifyContent: 'center'
    },
    tabButtonIcon: {
        alignSelf: 'center'
    }
});

export default FooterTab;