import React from 'react';
import {Image, View, Text, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


// Assets
import sampleImage from './sample-image';

class HomeFavoriteList extends React.Component {
    render() {
        return (
            <View style={{...styles.outerView, backgroundColor: (this.props.noBackground ? 'transparent' : '#a14b80')}}>
                <View style={styles.leftView}>
                    <Image style={styles.leftViewImage} source={{uri: sampleImage}} />
                </View>
                <View style={styles.middleView}>
                    <Text style={styles.middleViewText1}>Aramburu</Text>
                    <Text style={styles.middleViewText2}>
                        Pasaje del correo, Vicente
                        Lopez 1661, C1103 ACY,
                        Buenos Aires
                    </Text>
                </View>
                <View style={styles.rightView}>
                    <Image style={styles.rightViewImage} source={require('../../assets/home-favorites/favorite-list/heart.png')} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        height: hp('20%'),
        backgroundColor: '#a14b80',
        padding: wp('3%'),
        flexDirection: 'row',
        margin: 0,
        width: '100%'
    },
    leftView: {
        flex: 1
    },
    leftViewImage: {
        width: '100%',
        height: '100%',
        borderRadius: wp('3%')
    },
    middleView: {
        flex: 2,
        paddingLeft: wp('2.5%'),
        paddingRight: wp('2.75%')
    },
    middleViewText1: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: wp('5%')
    },
    middleViewText2: {
        color: '#ffffff',
        fontSize: wp('3%')
    }
});

export default HomeFavoriteList;