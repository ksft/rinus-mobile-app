import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class EnableLocation extends React.Component {
    render() {
        return (
            <View style={styles.outerOverlay}>
                <View style={styles.outerPopup}>
                    <View style={{paddingBottom: 10}}>
                        <Image 
                            style={{alignSelf: 'center', height: hp('5%'), width: wp('6.5%')}}
                            source={require('../../assets/misc/location-marker.png')} 
                        />
                    </View>
                    <View style={{paddingBottom: 10}}>
                        <Text style={{fontSize: wp('6%'), alignSelf: 'center'}}>Enable Location</Text>
                    </View>
                    <View style={{paddingBottom: 30}}>
                        <Text style={{fontSize: wp('3%'), alignSelf: 'center', color: '#6a6a6a'}}>Enable your location to show nearby places.</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around', paddingBottom: 10}}>
                        <View style={styles.actionBottonView}>
                            <TouchableOpacity style={styles.actionBotton}>
                                <Text style={styles.actionBottonText}>Later</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.actionBottonView}>
                            <TouchableOpacity style={{...styles.actionBotton, backgroundColor: '#b85692'}}>
                                <Text style={{...styles.actionBottonText, color: '#ffffff'}}>Enable</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    outerOverlay: {
        backgroundColor: "#000000aa",
        position: 'absolute',
        top: 0,
        left: 0,
        width: wp('100%'),
        height: hp('100%'),
        zIndex: 10,
        elevation: 10,
        justifyContent: 'center',
    },
    outerPopup: {
        backgroundColor: '#ffffff',
        width: wp('90%'),
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 5,
        padding: 20
    },
    actionBotton : {
        borderWidth: 0.7,
        borderColor: '#b85692',
        padding: 10,
        paddingRight: 20,
        paddingLeft: 20,
        borderRadius: 8,
        justifyContent: 'center',
    },
    actionBottonView: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    actionBottonText: {
        alignSelf: 'center',
        color: '#b85692',
        textTransform: 'uppercase'
    }
});

export default EnableLocation;