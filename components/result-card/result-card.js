import React from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import NavigationService from '../../src/NavigationService';


class ResultCard extends React.Component {
    render() {
        return (
            <TouchableOpacity onPress={() => {
                NavigationService.navigate('Details');
            }}>
                <View style={{...styles.outerView, ...this.props.style}}>
                    <View style={{...styles.section, ...styles.sectionU}}>
                        <Image 
                            style={styles.cardImage}
                            source={require('../../assets/listing-page/result-card-sample-image.png')}
                        />
                    </View>
                    <View style={{...styles.section, ...styles.sectionL}}>
                        <Text style={styles.headerText}>Aramburu</Text>
                        <Text style={styles.descriptionText}>
                            Pasaje del correo, Vicente
                            Lopez 1661, C1103 ACY,
                            Buenos Aires
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        backgroundColor: '#ffffff',
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 10,
        height: hp('30%')
    },
    section: {
        flex: 1,
        overflow: 'hidden'
    },
    sectionU: {
        flex: 1.5,
        overflow: 'hidden'
    },
    sectionL: {
        flex: 1,
        padding: wp('2%'),
        paddingBottom: 25
    },
    headerText: {
        fontSize: wp('5%'),
        fontWeight: 'bold',
        marginBottom: 6
    },
    descriptionText: {
        fontSize: wp('3.5%')
    },
    cardImage: {
        flex: 1,
        width: undefined,
        height: undefined,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    }
});

export default ResultCard;