import React from 'react';
import {Image} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const leftArrow = require('../../assets/misc/left-arrow.png');
const rightArrow = require('../../assets/misc/right-arrow.png');

class CalendarArrow extends React.Component {
    render() {
        return (
            <Image 
                style={{
                    height: hp('5%'),
                    width: wp('4%')
                }}
                source={
                    this.props.direction === "left"? leftArrow : rightArrow
                }
            />
        );
    }
}

export default CalendarArrow;