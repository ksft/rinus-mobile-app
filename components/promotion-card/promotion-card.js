import React from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


// assets
import sampleImage from './sample-image';

class PromotionCard extends React.Component {
    render() {
        return (
            <View>
                <TouchableOpacity style={styles.closeView}>
                    <Image 
                        style={styles.closeViewImage}
                        source={require('../../assets/promotion-card/close_icon.png')}
                    />
                </TouchableOpacity>
                
                <View style={styles.cardOuter}>
                    <View style={styles.imageView}>
                        <Image
                            style={styles.imageViewImage}
                            source={{ uri: sampleImage}} />
                    </View>
                    <View style={styles.infoView}>
                        <View>
                            <Text style={styles.infoViewText1}>50% OFF</Text>
                        </View>
                        <View>
                            <Text style={styles.infoViewText2}>at LaCabrera</Text>
                        </View>
                        <View>
                            <Text style={styles.infoViewText3}>+ Free drink</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardOuter: {
        backgroundColor: '#ffffff',
        margin: 10,
        borderRadius: 10,
        padding: 10,
        width: wp('50%'),
        height: hp('12%'),
        flexDirection: 'row',
        overflow: 'hidden'
    },
    imageView: {
        paddingBottom: 20,
        overflow: 'hidden',
        borderRadius: 7
    },
    imageViewImage: { 
        width: wp('15%'), 
        height: hp('10%'), 
        borderRadius: 7 
    },
    infoView: {
        paddingLeft: 10    
    },
    infoViewText1: {
        color: '#b75692',
        fontSize: wp('5%'),
        fontWeight: 'bold'
    },
    infoViewText2: {
        color: '#000000',
        fontSize: wp('3%'),
        fontWeight: 'bold'
    },
    infoViewText3: {
        color: '#000000',
        fontSize: wp('2%'),
    },
    closeView: {
        width: wp('4%'),
        height: hp('5%'),
        position: 'absolute',
        left: wp('45%'),
        top: -hp('0.5%'),
        zIndex: 1,
        elevation: 1 
    },
    closeViewImage: {
        flex: 1,
        height: undefined,
        width: undefined
    }
});

export default PromotionCard;