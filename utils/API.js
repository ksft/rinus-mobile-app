import axios from 'axios';

export default axios.create({
    baseURL: "http://165.22.12.218/api",
    responseType: "json"
});