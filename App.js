import React from 'react';
import {
	I18nManager,
	StatusBar,
} from 'react-native';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import SplashScreen from 'react-native-splash-screen';

import EnableLocation from './components/enable-location/enable-location';
import FooterTab from './components/footer-tab/footer-tab';
import AppNavigator from './src/AppNavigator';
import NavigationService from './src/NavigationService';

// Localization
const translationGetters = {
	en: () => require('./src/translations/en.json'),
	es: () => require('./src/translations/es.json'),
	pt: () => require('./src/translations/pt.json'),
}

global.translate = memoize(
	(key, config) => i18n.t(key, config),
	(key, config) => (config ? key + JSON.stringify(config): key)
);

const setI18nConfig = () => {
	const fallback = {languageTag: "en", isRTL: false};

	const {languageTag, isRTL} = RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) || fallback;

	translate.cache.clear();
	I18nManager.forceRTL(isRTL);

	i18n.translations = { [languageTag]: translationGetters[languageTag]() };
	i18n.locale = languageTag;
}
// End Localization

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			loggedIn: false
		}
		setI18nConfig();
		this.state = {
			showEnableLocation: false
		}
	}

	openEnableLocation() {
		this.setState({
			showEnableLocation: true
		});
	}
	
	closeEnableLocation() {
		this.setState({
			showEnableLocation: false
		});
	}

	componentDidMount() {
		SplashScreen.hide();
		RNLocalize.addEventListener("change", this.handleLocalizationChange);
	}

	componentWillUnmount() {
		RNLocalize.removeEventListener("change", this.handleLocalizationChange);
	}

	handleLocalizationChange = () => {
		setI18nConfig();
		this.forceUpdate();
	}

	render() {
		return (
			<>
				{/* <StatusBar barStyle="light-content" backgroundColor="#724d8b" /> */}
                <StatusBar translucent backgroundColor="transparent" />
				<AppNavigator 
					ref={navigatorRef => {
          				NavigationService.setTopLevelNavigator(navigatorRef);
        			}} 
				/>
				{/* <FooterTab /> */}
			</>
		);
	}
}

export default App;
