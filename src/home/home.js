import React from 'react';
import { Image, Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';


// Components
import PromotionCard from '../../components/promotion-card/promotion-card';
import HomeFavorites from './home-favorites';
import HomeCategory from './home-category';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import FooterTab from '../../components/footer-tab/footer-tab';


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            screenPage: 1
        }
    }

    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <View style={{...styles.sections, ...styles.sectionPromotionText}}>
                        <Text style={{color: '#b499c4', fontWeight: '100', fontSize: wp('2%')}}>{translate("promotions")}</Text>
                    </View>
                    <View style={{...styles.sections, ...styles.sectionPromotionView}}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                            <PromotionCard />
                        </ScrollView>
                    </View>

                    <View style={{...styles.sections, ...styles.sectionCategory}}>
                        <View style={styles.sectionCategoryRow}>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'personal_care'} 
                                    text={'Personal Care'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'medicine'} 
                                    text={'Medicine'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'pet_care'} 
                                    text={'Pet Care'} 
                                />
                            </View>
                        </View>

                        <View style={styles.sectionCategoryRow}>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'odontology'} 
                                    text={'Odontology'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'sports'} 
                                    text={'Sports'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'car_service'} 
                                    text={'Car Service'} 
                                />
                            </View>
                        </View>

                        <View style={styles.sectionCategoryRow}>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'music'} 
                                    text={'Music'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'professional_service'} 
                                    text={'Professional Services'} 
                                />
                            </View>
                            <View style={styles.sectionCategoryCol}>
                                <HomeCategory 
                                    image={'restaurants'} 
                                    text={'Restaurants'} 
                                />
                            </View>
                        </View>
                    </View>
                </View>

                <View>
                    <HomeFavorites />
                </View>
                <FooterTab link={this.state.screenPage} />
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    sections: {
    },
    sectionPromotionText: {
        flex: 0.15,
        justifyContent: "flex-end"
    },
    sectionCategory: {
        flex: 2,
        paddingBottom: hp('16%'),
    },
    sectionCategoryRow: {
        flex: 1,
        flexDirection: 'row',
    },
    sectionCategoryCol: {
        alignItems: 'center',
        flexGrow: 1,
        width: '33%',
        padding: 7
    },
    sectionPromotionView: {
        flex: 0.6,
        paddingBottom: 2,
        overflow: 'visible'
    }
});

export default Home;