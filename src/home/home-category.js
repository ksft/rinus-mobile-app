import React from 'react';
import {
    View, 
    Text, 
    Image, 
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


let images = {
    personal_care: require('../../assets/categories/personal_care_icon.png'),
    medicine: require('../../assets/categories/medicine_icon.png'),
    pet_care: require('../../assets/categories/pet_care_icon.png'),
    odontology: require('../../assets/categories/odontology_icon.png'),
    sports: require('../../assets/categories/sports_icon.png'),
    car_service: require('../../assets/categories/car_services_icon.png'),
    music: require('../../assets/categories/music_icon.png'),
    professional_service: require('../../assets/categories/professional_services_icon.png'),
    restaurants: require('../../assets/categories/restaurants_icon.png')
}

class HomeCategory extends React.Component {
    render() {
        let {image, text} = this.props;
        return (
            <TouchableOpacity style={styles.outerView}>
                <View>
                    <View style={styles.categoryImageView}>
                        <Image
                            style={styles.categoryImage} 
                            source={images[image]} 
                        />
                    </View>
                    <View style={styles.categoryTextView}>
                        <Text style={styles.categoryText}>{translate(image)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
    },
    categoryImageView: {
        overflow: 'hidden',
        flex: 1
    },
    categoryImage: {
        width: wp('12%'),
        height: '100%',
        alignSelf: 'center'
    }, 
    categoryTextView: {
        flex: 0.3
    },
    categoryText: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: wp('3.5%')
    }
});

export default HomeCategory;