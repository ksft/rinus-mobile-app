import React from 'react';
import {Animated, View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, PanResponder} from 'react-native';


// Components
import HomeFavoriteList from '../../components/home-favorite-list/home-favorite-list';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const _3_by_4_height = (3/4) * hp('100%');

class HomeFavorites extends React.Component {
    constructor(props) {
        super(props);

        const panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderMove: (event, gesture) => {
                let {outerViewHeight} = this.state;
                let moveY = hp('100%') - gesture.moveY;
                let heightValue = moveY;
                if(heightValue > hp('95%')) {
                    heightValue = hp('95%');
                }
                if(heightValue < hp('14%')) {
                    heightValue = hp('14%');
                }
                outerViewHeight.setValue(heightValue);
            },

            onPanResponderRelease : (event, gesture) => {
                let {outerViewHeight} = this.state;

                if (outerViewHeight._value > _3_by_4_height) {
                    Animated.timing(this.state.outerViewHeight,{
                        toValue: hp('95%'),
                        duration: 100
                    }).start();
                }
                else {
                    Animated.timing(this.state.outerViewHeight,{
                        toValue: hp('14%'),
                        duration: 100
                    }).start();
                }
            }
        });

        this.state = {
            outerViewHeight: new Animated.Value(hp('14%')),
            darkOverlayDisplayValue: 'flex',
            darkOverlayHeight: new Animated.Value(0),
            panResponder
        }
    }
  
    render() {
        const config = {
            velocityThreshold: 0.2,
            directionalOffsetThreshold: 100
        };
        let {darkOverlayDisplayValue} = this.state;
        return (
            <React.Fragment>
                <Animated.View style={[styles.outerView, {height: this.state.outerViewHeight}]}>
                        <Animated.View style={{...styles.darkOverlay, display: darkOverlayDisplayValue, height: this.state.darkOverlayHeight}}></Animated.View>
                    <View style={{...styles.innerUpperView, backgroundColor: "#b75690",  borderTopLeftRadius: 20,
        borderTopRightRadius: 20}} {...this.state.panResponder.panHandlers}>
                    <TouchableOpacity>
                        <View style={styles.innerUpperView1}>
                            <Image 
                                source={require('../../assets/home-favorites/drag_bar.png')} 
                            />
                        </View>
                        <View style={styles.innerUpperView2}>
                            <View style={styles.innerUpperView2Left}>
                                <Text style={styles.innerUpperView2LeftText}>
                                    {translate("favorites")}
                                </Text>
                            </View>
                            <View style={styles.innerUpperView2Right}>
                                <Text style={styles.innerUpperView2RightText}>5</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    </View>

                    <View style={styles.innerLowerView}>
                        <ScrollView style={{padding: 0, margin: 0, width: '100%', backgroundColor: "#b75690"}} showsVerticalScrollIndicator={false}>
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                            <HomeFavoriteList />
                            <HomeFavoriteList noBackground />
                        </ScrollView>
                    </View>
                </Animated.View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        padding: 0,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        flex: 1
    },
    innerUpperView1: {
        alignItems: 'center',
        paddingTop: 10
    },
    innerUpperView2LeftText: {
        color: '#ffffff',
        fontSize: wp('3%'),
        top: 5,
        left: 5,
        marginBottom: hp('2%')
    },
    innerUpperView2: {
        padding: 10,
        flexDirection: 'row'
    },
    innerUpperView2Left: {
        flex: 3
    },
    innerUpperView2Right: {
        flex: 1,
        alignItems: 'flex-end'
    },
    innerUpperView2RightText: {
        backgroundColor: '#994679',
        color: '#ffffff',
        paddingLeft: wp('2.5%'),
        paddingRight: wp('2.5%'),
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        borderRadius: 100,
        fontSize: wp('3%'),
        bottom: 7,
        right: 5
    },
    innerLowerView: {
        flex: 1,
        padding: 0,
        margin: 0,
        paddingBottom: 50
    },
    darkOverlay: {
        backgroundColor: '#000000ab',
        width: '100%',
        height: hp('25%'),
        opacity: 1,
        marginBottom: -15
    }
});

export default HomeFavorites;