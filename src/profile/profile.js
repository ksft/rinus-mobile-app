import React from 'react';
import {Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import SwitchToggle from 'react-native-switch-toggle';
import FooterTab from '../../components/footer-tab/footer-tab';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activateNotification: false,
            serviceFor: 'men'
        };
    }

    render() {
        return (
            <React.Fragment>
            <ScrollView style={styles.screen}>
                {/* Pofile Picture */}
                <View style={{...styles.section, ...styles.profileNameSection}}>
                    <Image 
                        style={styles.profileNameImage}
                        source={require('../../assets/profile-page/profile-picture.png')}
                    />
                    <Text style={styles.profileNameText}>Robert John</Text>
                </View>
                {/* details */}
                <View style={{...styles.section, paddingTop: 40}}>
                    <View style={styles.detailsSection}>
                        <Image
                            style={styles.searchInputIcon}
                            source={require("../../assets/misc/envelope.png")}
                        />
                        <TextInput
                            style={{...styles.searchInput, color: '#ffffff'}}
                            placeholder="EMAIL ADDRESS"
                            placeholderTextColor="#ffffffa0"
                        />
                    </View>
                    <View style={styles.detailsSection}>
                        <Image
                            style={{...styles.searchInputIcon, height: hp('5%'), width: wp('5.7%'), top:5}}
                            source={require("../../assets/misc/phone.png")}
                        />
                        <TextInput
                            style={styles.searchInput}
                            placeholder="PHONE NUMBER"
                            placeholderTextColor="#ffffffa0"
                        />
                    </View>
                    <View style={styles.detailsSection}>
                        <Image
                            style={{...styles.searchInputIcon, height: hp('5%'), width: wp('8.1%'), top:5}}
                            source={require("../../assets/misc/gift.png")}
                        />
                        <TextInput
                            style={styles.searchInput}
                            placeholder="BIRTHDAY"
                            placeholderTextColor="#ffffffa0"
                        />
                    </View>
                </View>
                
                <View style={{...styles.section, ...styles.switchSection}}>
                    <View>
                        <Text style={styles.switchSectionText}>Activate Notifications</Text>
                    </View>
                    <View>
                        <SwitchToggle 
                            backgroundColorOn="#b85692"
                            switchOn={this.state.activateNotification}
                            circleColorOn="#ffffff"
                            containerStyle={{
                                marginTop: 16,
                                width: 50,
                                height: 30,
                                borderRadius: 20,
                                backgroundColor: '#ccc',
                                padding: 0,
                              }}
                            onPress={() => {this.setState({activateNotification: !this.state.activateNotification})}}
                        />
                    </View>
                </View>
                {/* Service choice */}
                <View style={{...styles.section, borderBottomColor: '#855fa0',
        borderBottomWidth: 2}}>
                    <View style={styles.section}>
                        <Text style={{color: '#ffffff'}}>Show me first services for</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <View style={styles.serviceRadioView}>
                            <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceFor == 'women') ? '#b85692' : '#b8569200')}}
                                onPress={() => {this.setState({serviceFor: 'women'})}}
                            >
                                <Text style={{...styles.serviceRadioText, color: ((this.state.serviceFor == 'women') ? '#ffffff' : '#c49ede')}}>WOMEN</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.serviceRadioView}>
                            <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceFor == 'men') ? '#b85692' : '#b8569200')}}
                            onPress={() => {this.setState({serviceFor: 'men'})}}
                            >
                                <Text style={{...styles.serviceRadioText, color: ((this.state.serviceFor == 'men') ? '#ffffff' : '#c49ede')}}>MEN</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.serviceRadioView}>
                            <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceFor == 'both') ? '#b85692' : '#b8569200')}}
                            onPress={() => {this.setState({serviceFor: 'both'})}}
                            >
                                <Text style={{...styles.serviceRadioText, color: ((this.state.serviceFor == 'both') ? '#ffffff' : '#c49ede')}}>BOTH</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {/* Feedback and Support Link */}
                <View style={{...styles.section, borderBottomColor: '#855fa0',
        borderBottomWidth: 2}}>
                    <TouchableOpacity>
                        <View style={styles.fnsLink}>
                            <View>
                                <Text style={styles.switchSectionText}>Feedback &amp; Support</Text>
                            </View>
                            <View>
                                <Image 
                                    source={require('../../assets/misc/right-arrow.png')}
                                    style={{height: hp('5%')}}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                {/* Logout Link */}
                <View style={{...styles.section, borderBottomColor: '#855fa0',
        borderBottomWidth: 2}}>
                    <TouchableOpacity>
                        <View style={styles.fnsLink}>
                            <View>
                                <Text style={styles.switchSectionText}>Logout</Text>
                            </View>
                            <View>
                                {/* <Image 
                                    source={require('../../assets/misc/right-arrow.png')}
                                    style={{height: hp('5%')}}
                                /> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{height: hp('40%')}}></View>
                
            </ScrollView>
            <FooterTab link={4} />
            </React.Fragment>

        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    section: {
        paddingTop: 16,
        paddingBottom: 16
    },

    profileNameSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profileNameImage: {
        width: wp('15%'),
        height: wp('15%'),
        marginRight: 20
    },
    profileNameText: {
        color: '#ffffff',
        fontSize: wp('7%')
    },
    searchInputIcon: {
        position: 'absolute',
        top: 13,
        left: 0,
        width: wp('7%'),
        height: hp('3%')
    },
    searchInput: {
        paddingLeft: 45,
        borderBottomColor: '#855fa0',
        borderBottomWidth: 1,
        marginBottom: 10
    },
    switchSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#855fa0',
        borderBottomWidth: 2,
    },
    switchSectionText: {
        color: '#ffffff',
        fontSize: wp('4%')
    },
    serviceRadioView: {
        flex: 1,
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5
    },
    serviceRadio: {
        backgroundColor: '#603c78',
        padding: 10,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1
    },
    serviceRadioText: {
        color: '#c49ede',
        alignSelf: 'center'
    },
    fnsLink: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }

});

export default Profile;