import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NavigationService from '../NavigationService';

class ServiceTag extends React.Component {
    render() {
        return (
            <View style={styles.outerView}>
                <View style={{flex: 2}}>
                    <Text style={{color: '#ffffff'}}>Haircut</Text>
                </View>
                <View style={{flex: 1}}>
                    <Text style={{color: '#ffffff', fontWeight: 'bold'}}>$20.00</Text>
                    <Text style={{color: '#855fa0'}}>40m</Text>
                </View>
                <View style={{flex: 1}}>
                    <TouchableOpacity style={styles.bookButton} 
                        onPress={() => {NavigationService.navigate('BookingSelect')}}
                    >
                        <Text style={{color: '#ffffff', alignSelf: 'center'}}>BOOK</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        flexDirection: 'row',
        height: hp('10%'),
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#855fa0'
    },
    bookButton: {
        backgroundColor: '#b85692',
        borderRadius: 5,
        padding: 10
    }
});

export default ServiceTag;