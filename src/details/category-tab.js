import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class CategoryTab extends React.Component {
    render() {
        return (
            <TouchableOpacity style={styles.outerView} onPress={() => { this.props.onPress(); }}>
                {/* <View style={styles.outerView}> */}
                    <Text style={{...styles.categoryText, color: (this.props.active ? "#ffffff": "#ffffffa0")}}>{this.props.categoryText}</Text>
                {/* </View> */}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        padding: 12,
        paddingTop: 15
    },
    categoryText: {
        color: "#ffffffa0",
        fontSize: wp('3.5%'),
        textTransform: 'uppercase',
        alignSelf: 'center'
    }
});

export default CategoryTab;