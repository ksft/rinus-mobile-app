import React from 'react';
import { Button, Image, Text, View, StyleSheet, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Calendar, CalendarList, Agenda} from 'react-native-calendars';
import Staffer from '../../components/staffer/staffer';
import CalendarArrow from '../../components/calendar-arrow/calendar-arrow';

class BookingSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceTime: '10:00 AM'
        };
    }

    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.navView}>
                            <TouchableOpacity 
                                style={styles.navViewButton}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image
                                    style={styles.navViewImage}
                                    source={require('../../assets/misc/back-arrow.png')}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.headingView}>
                            <Text style={styles.headingViewText}>Select for Booking </Text>
                        </View>

                        <View style={{paddingTop: 25}}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <Staffer selectable />                                             
                                <Staffer selectable />
                                <Staffer selectable />
                                <Staffer selectable />
                                <Staffer selectable />
                                <Staffer selectable />
                                <Staffer selectable />
                                <Staffer selectable />
                            </ScrollView>
                        </View>

                        <View style={{paddingTop: 25}}>
                            <Calendar 
                                theme={calendarTheme}
                                renderArrow={(direction) => <CalendarArrow direction={direction} />}
                            />
                        </View>

                        <View style={{paddingTop: 25}}>
                            <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '10:00 AM') ? '#b85692' : '#b8569200')}}
                                        onPress={() => {this.setState({serviceTime: '10:00 AM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '10:00 AM') ? '#ffffff' : '#c49ede')}}>10:00 AM</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '10:30 AM') ? '#b85692' : '#b8569200')}}
                                    onPress={() => {this.setState({serviceTime: '10:30 AM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '10:30 AM') ? '#ffffff' : '#c49ede')}}>10:30 AM</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '11:00 AM') ? '#b85692' : '#b8569200')}}
                                    onPress={() => {this.setState({serviceTime: '11:00 AM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '11:00 AM') ? '#ffffff' : '#c49ede')}}>11:00 AM</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '11:30 AM') ? '#b85692' : '#b8569200')}}
                                        onPress={() => {this.setState({serviceTime: '11:30 AM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '11:30 AM') ? '#ffffff' : '#c49ede')}}>11:30 AM</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '12:00 PM') ? '#b85692' : '#b8569200')}}
                                    onPress={() => {this.setState({serviceTime: '12:00 PM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '12:00 PM') ? '#ffffff' : '#c49ede')}}>12:00 PM</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.serviceRadioView}>
                                    <TouchableOpacity style={{...styles.serviceRadio, borderColor: ((this.state.serviceTime == '12:30 AM') ? '#b85692' : '#b8569200')}}
                                    onPress={() => {this.setState({serviceTime: '12:30 AM'})}}
                                    >
                                        <Text style={{...styles.serviceRadioText, color: ((this.state.serviceTime == '12:30 AM') ? '#ffffff' : '#c49ede')}}>12:30 AM</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                            
                            <View style={{paddingTop: 25}}>
                                <View style={{
                                    flexDirection: 'row',
                                    height: hp('10%'),
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                    borderBottomColor: '#855fa0'
                                }}>
                                    <View style={{flex: 1}}>
                                        <Text style={{color: '#ffffff'}}>Haircut</Text>
                                        <Text style={{color: '#855fa0'}}>Robert John</Text>
                                    </View>
                                    <View style={{flex: 1, alignItems: "flex-end"}}>
                                        <Text style={{color: '#ffffff', fontWeight: 'bold'}}>$20.00</Text>
                                        <Text style={{color: '#855fa0'}}>10:00 AM - 10:30 AM</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={{paddingTop: 25}}>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    borderBottomColor: '#855fa0',
                                    borderBottomWidth: 2,
                                    paddingBottom: 25
                                }}>
                                    <Image 
                                        style={{width: wp('7%'), height: hp('4%')}}
                                        source={require('../../assets/misc/round-plus.png')} />
                                    <Text style={{color: '#d46eac', fontSize: wp('4.5%')}}> &nbsp; ADD ANOTHER SERVICE </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{paddingTop: 25}}>
                                <View>
                                    <Image 
                                        style={{
                                            position: 'absolute',
                                            height: hp('5%'),
                                            width: wp('7%')
                                        }} 
                                        source={require('../../assets/misc/notes.png')} 
                                    />
                                    <TextInput
                                        style={styles.searchInput}
                                        placeholder="LEAVE A NOTE (OPTIONAL)"
                                        placeholderTextColor = "#ffffffa0"
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={{paddingTop: 25}}>
                            <TouchableOpacity 
                                style={styles.linkViewContinue}
                                onPress={() => this.props.navigation.navigate('Signup')}
                            >
                                <Text style={styles.linkViewContinueText}>CONFIRM</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height: hp('20%')}}></View>
                    </ScrollView>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    
    navView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    navViewButton: {
        width: wp('12%'),
        height: hp('7.7%'),
        padding: 10
    },
    navViewImage: {
        flex: 1,
        width: undefined,
        height: undefined
    },
    
    headingView: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        justifyContent: 'flex-start'
    },
    headingViewText: {
        color: '#ffffff',
        fontSize: wp('9%')
    },
    
    formView: {
        flex: 2,
        paddingLeft: 10,
        justifyContent: 'flex-end'
    },
    searchInputIcon: {
        position: 'absolute',
        top: 13,
        left: 0,
        width: wp('7%'),
        height: hp('3%')
    },
    searchInput: {
        paddingLeft: 45,
        borderBottomColor: '#855fa0',
        borderBottomWidth: 2,
        color: '#ffffff'
    },
    
    linkView: {
        flex: 3,
        padding: 10
    },
    linkViewContinue: {
        padding: 12,
        backgroundColor: '#b85692',
        borderRadius: 5,
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    }, 
    linkViewContinueText: {
        color: '#ffffff',
        alignSelf: 'center',
        // fontWeight: 'bold',
        fontSize: wp('5%')
    },
    linkViewFB: {
        padding: 12,
        backgroundColor: '#2775b7',
        borderRadius: 5,
        justifyContent: 'center',
        flexDirection: 'row',
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    linkViewFBIcon: {
        height: hp('3%'),
        width: wp('4%'),
        marginRight: 20,
        top: 5
    },
    serviceRadioView: {
        flex: 1,
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5
    },
    serviceRadio: {
        backgroundColor: '#603c78',
        padding: 10,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1
    },
    serviceRadioText: {
        color: '#c49ede',
        alignSelf: 'center'
    }
});

const calendarTheme = {
    calendarBackground: "#00000000",
    arrowColor: "#9371a9",
    todayBackgroundColor: "#b85692",
    todayTextColor: "#ffffff",
    monthTextColor: "#ffffff",
    textMonthFontWeight: 'bold',
}

export default BookingSelect;