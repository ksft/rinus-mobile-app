import React from 'react';
import {Animated, Image, ImageBackground, PanResponder, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import CategoryTab from './category-tab';
import ServiceTag from './service-tag';
import Staffer from '../../components/staffer/staffer';

class Details extends React.Component {
    constructor(props) {
        super(props);

        const panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderMove: (event, gesture) => {
                let {upperHeight} = this.state;
                let moveY = gesture.moveY;
                let heightValue = moveY;
                if(heightValue < hp('15%')) {
                    heightValue = hp('15%');
                }
                if(heightValue > hp('40%')) {
                    heightValue = hp('40%');
                }
                upperHeight.setValue(heightValue);
            },

            onPanResponderRelease : (event, gesture) => {
                
            }
        });

        this.state = {
            upperHeight: new Animated.Value(hp('40%')),
            activeSection: 'services',
            panResponder
        }
    }
    render() {
        return (
            <React.Fragment>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={styles.screen}>
                    <Animated.View style={{...styles.section, height: this.state.upperHeight, marginBottom: -40}}>
                        <ImageBackground 
                            source={require('../../assets/listing-page/details-sample-head.png')}
                            style={{flex: 1, width: undefined, height: undefined}}
                        >
                            <View style={{padding: 25, paddingLeft: 16, paddingRight: 16, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.goBack();
                                    }}
                                >
                                    <Image 
                                        style={styles.navImage}
                                        source={require('../../assets/misc/back-arrow.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image 
                                        style={{...styles.navImage, right: 20}}
                                        source={require('../../assets/listing-page/hollow-heart.png')} />
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </Animated.View>

                    <View style={{...styles.section, height: hp('95%'), marginBottom: -40}}>
                        <View {...this.state.panResponder.panHandlers}>
                            <View style={styles.infoCard}>
                                <Text style={styles.infoCardHeading}>D &amp; D Salon Unisex</Text>
                                <Text style={styles.infoCardDescr}>Aeronautica Argentina 1296, B1744ICB Trujui, Buenos Aires, Argentina</Text>
                            </View>
                        </View>

                        <View style={{...styles.tabSection}}>
                            <CategoryTab categoryText="SERVICES" 
                                active={(this.state.activeSection === "services") ? true : false} 
                                onPress={() => { 
                                    this.setState({
                                        activeSection: "services"
                                    });
                                }} 
                            />
                            <CategoryTab categoryText="INFO" 
                                active={(this.state.activeSection === "info") ? true : false} 
                                onPress={() => { 
                                    this.setState({
                                        activeSection: "info"
                                    });
                                }} 
                            />
                        </View>

                        { (this.state.activeSection === 'services') 
                            ? 
                            /* Services */
                            <View>
                                <ScrollView style={{paddingLeft: 16, paddingRight: 16}}>
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                    <ServiceTag />
                                </ScrollView>
                            </View>
                            :
                            /* Info */
                            <View style={{flex: 1}}>
                                <ScrollView style={{flex: 1}}>
                                    <View style={styles.mapView}>
                                        <MapView
                                            style={styles.mapView}
                                            provider={PROVIDER_GOOGLE}
                                            initialRegion={{
                                                latitude: 37.78825,
                                                longitude: -122.4324,
                                                latitudeDelta: 0.0922,
                                                longitudeDelta: 0.0421,
                                                zoom: 15
                                            }}
                                            
                                        >
                                            <Marker 
                                                coordinate={{latitude: 37.78825, longitude: -122.4324}} 
                                                title={"D & D Salon Unisex"}
                                                image={require('../../assets/misc/location-marker.png')}
                                            />
                                        </MapView>
                                    </View>
                                    <View style={{padding: 16}}>
                                        <Text style={{color: '#dabcee'}}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et scelerisque nibh. Suspendisse vitae risus massa. Nulla nibh lorem, malesuada eget egestas vel, euismod eget quam. Aenean molestie, lectus vitae euismod laoreet, eros orci sollicitudin eros, nec interdum ante orci sed lorem. Phasellus venenatis dolor odio, in sodales enim sodales eu. Phasellus semper venenatis dui, at efficitur libero sodales vel. In cursus magna felis, at bibendum libero dictum tempor. Sed mi arcu, placerat eget nibh nec, pulvinar iaculis velit. Aenean dictum auctor nisi.
                                        </Text>

                                        <Text style={{color: '#ffffff', paddingTop: 16, fontSize: wp('3%'), marginTop: 25}}>
                                            STAFFERS
                                        </Text>

                                        <View style={{paddingTop: 25}}>
                                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                                <Staffer />                                             
                                                <Staffer />
                                                <Staffer />
                                                <Staffer />
                                                <Staffer />
                                                <Staffer />
                                                <Staffer />
                                                <Staffer />
                                            </ScrollView>
                                        </View>

                                        <View style={{paddingTop: 20}}>
                                            <View style={{...styles.infoCard, flexDirection: 'row'}}>
                                                <View style={{flex: 2}}>
                                                    <Text style={styles.infoCardHeading}>Contact Info</Text>
                                                    <Text style={styles.infoCardDescr}>0790701256</Text>
                                                </View>
                                                <View style={{flex: 1}}>
                                                    <TouchableOpacity style={styles.callButton}>
                                                        <Text style={{color: '#ffffff', alignSelf: 'center'}}>CALL</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>

                                        <Text style={{color: '#ffffff', paddingTop: 16, fontSize: wp('3%'), marginTop: 25}}>
                                            BUSINESS HOURS
                                        </Text>

                                        <View style={{paddingTop: 20}}>
                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Monday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>

                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Tuesday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>
                                            
                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Wednesday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>

                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Thursday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>

                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Friday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>

                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Saturday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>

                                            <View style={styles.businesHoursList}>
                                                <Text style={{fontSize: wp('4%'), color: '#dabcee'}}>Sunday</Text>
                                                <Text style={{fontSize: wp('4%'), color: '#ffffff'}}>10:00 AM - 7:00 PM</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {/* <View style={{height: hp('20%')}}></View> */}
                                </ScrollView>
                            </View>
                        }
                    </View>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
    },
    section: {
        /* flex: 1, */
        /* Delete Later */
        /* borderWidth: 1 */
    },
    infoCard: {
        backgroundColor: '#ffffff',
        alignSelf: 'center',
        width: wp('90%'),
        padding: 20,
        borderRadius: 12,
        marginBottom: 30
    },
    infoCardHeading: {
        fontWeight: 'bold',
        fontSize: wp('5%')
    },
    tabSection: {
        backgroundColor: '#5d3b73',
        flexDirection: 'row',
        justifyContent: "space-around",
        padding: 10,
        paddingTop: 8
    },
    navImage: {
        width: wp('8.5%'),
        height: wp('8%')
    },
    mapView: {
        height: hp('25%'),
        width: wp('100%'),
    },
    callButton: {
        backgroundColor: '#b85692',
        borderRadius: 5,
        padding: 10
    },
    businesHoursList: {flexDirection: 'row', justifyContent: 'space-between',borderBottomWidth: 1,
        borderBottomColor: '#855fa0', paddingBottom: 20, paddingTop: 20}
});

export default Details;