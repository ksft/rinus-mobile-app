import React from 'react';
import { View, Image, Text, StatusBar, StyleSheet, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

// Components
import ResultCard from '../../components/result-card/result-card';

class ListingMap extends React.Component {
    render() {
        return (
            <React.Fragment>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={styles.screen}>
                    {/* Map Area */}
                    <View style={styles.mapAreaView}>
                        <View style={styles.mapAreaSearchView}>
                            <View style={styles.mapSearchRow}>
                                <View style={styles.mapSearchCol1}>
                                    <View style={{flex: 1}}>
                                        <TouchableOpacity
                                            style={{...styles.mapSearchCol2Input, paddingTop: 10, paddingBottom: 10}}
                                            onPress={() => {
                                                this.props.navigation.goBack();
                                            }}
                                        >
                                            <Image 
                                                style={styles.mapSearchCol1Img}
                                                source={require('../../assets/misc/back-arrow-pink.png')} 
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.mapSearchCol2}>
                                    <View style={styles.mapSearchCol2Input}>
                                        <Image 
                                            style={styles.searchInputIcon} 
                                            source={require('../../assets/listing-page/search.png')} 
                                        />
                                        <TextInput
                                            style={styles.searchInput}
                                            placeholder="What are you looking for?"
                                            placeholderTextColor = "#000000a0"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.mapSearchRow}>
                                <View style={styles.mapSearchCol1}></View>
                                <View style={{...styles.mapSearchCol2, flexDirection: 'row'}}>
                                    <View style={styles.mapSearchCol2Input}>
                                        <Image 
                                            style={styles.searchInputIcon} 
                                            source={require('../../assets/listing-page/where.png')} 
                                        />
                                        <TextInput
                                            style={styles.searchInput}
                                            placeholder="Where?"
                                            placeholderTextColor = "#000000a0"
                                        />
                                    </View>
                                    <View style={styles.mapSearchCol2Input}>
                                        <Image 
                                            style={styles.searchInputIcon} 
                                            source={require('../../assets/listing-page/when.png')} 
                                        />
                                        <TextInput
                                            style={styles.searchInput}
                                            placeholder="When?"
                                            placeholderTextColor = "#000000a0"
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/* Map Render */}
                        <View style={{flex: 1}}>
                            <MapView
                                style={{flex: 1}}
                                provider={PROVIDER_GOOGLE}
                                initialRegion={{
                                    latitude: 37.78825,
                                    longitude: -122.4324,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                    zoom: 15
                                }}
                                
                            >
                                <Marker 
                                    coordinate={{latitude: 37.78825, longitude: -122.4324}} 
                                    title={"D & D Salon Unisex"}
                                >
                                    <Image 
                                        source={require('../../assets/misc/location-marker.png')} 
                                        style={{width: wp('15%'), height: hp('11.3%')}}
                                    />
                                </Marker>
                            </MapView>
                        </View>
                    </View>

                    {/* Result Area */}
                    <View style={styles.resultAreaView}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <ResultCard style={{marginRight: 20, marginLeft: 16}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                            <ResultCard style={{marginRight: 20}} />
                        </ScrollView>
                    </View>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
    },

    /* Map Area */
    mapAreaView: {
        flex: 2,
    },
    mapAreaSearchView: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: wp('100%'),
        zIndex: 99,
        elevation: 99,
        paddingTop: 25
    },
    mapSearchRow: {
        flexDirection: 'row'
    },
    mapSearchCol1: {
        flex: 1,
        padding: 5
    },
    mapSearchCol1Img: {
        width: wp('10%'),
        height: hp('4%')
    },
    mapSearchCol2: {
        flex: 3,
        padding: 5
    },
    mapSearchCol2Input: {
        backgroundColor: '#ffffff',
        flexGrow: 1,
        borderRadius: 500,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        marginTop: 0
    },
    searchInputIcon: {
        /* width: wp('4%'),
        height: hp('2%'), */
        width: 15,
        height: 15,
        position: 'absolute',
        top: hp('3.5%'),
        left: wp('4.5%'),  
    },
    searchInput: {
        borderColor: '#ffffff5f',
        borderWidth: 2,
        borderRadius: 10,
        margin: 5,
        padding: wp('2%'),
        paddingLeft: wp('9%'),
        height: hp('7.5%'),
        color: '#ffffff'
    },

    /* Result Area */
    resultAreaView: {
        borderWidth: 1,
        flex: 1
    }
});

export default ListingMap;