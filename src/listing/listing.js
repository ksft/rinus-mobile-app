import React from 'react';
import { View, Image, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// Components
import CategoryTab from './category-tab';
import ResultCard from '../../components/result-card/result-card';
import FooterTab from '../../components/footer-tab/footer-tab';

class Listing extends React.Component {
    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <View style={{...styles.section, ...styles.searchSection}}>
                        <View>
                            <Image 
                                style={styles.searchInputIcon} 
                                source={require('../../assets/listing-page/search.png')} 
                            />
                            <TextInput
                                style={styles.searchInput}
                                placeholder="What are you looking for?"
                                placeholderTextColor = "#ffffffa0"
                            />
                        </View>
                        <View style={styles.searchSectionL}>
                            <View style={styles.searchSectionLV}>
                                <Image 
                                    style={styles.searchInputIcon} 
                                    source={require('../../assets/listing-page/where.png')} 
                                />
                                <TextInput
                                    style={styles.searchInput}
                                    placeholder="Where?"
                                    placeholderTextColor = "#ffffffa0"
                                />
                            </View>
                            <View style={styles.searchSectionLV}>
                                <Image 
                                    style={styles.searchInputIcon} 
                                    source={require('../../assets/listing-page/when.png')} 
                                />
                                <TextInput
                                    style={styles.searchInput}
                                    placeholder="When?"
                                    placeholderTextColor = "#ffffffa0"
                                />
                            </View>
                        </View>
                    </View>

                    <View style={{...styles.tabSection}}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <CategoryTab categoryText="All" active />
                            <CategoryTab categoryText="Personal Care" />
                            <CategoryTab categoryText="Medicine" />
                            <CategoryTab categoryText="Pet Care" />
                            <CategoryTab categoryText="Odontology" />
                            <CategoryTab categoryText="Sports" />
                            <CategoryTab categoryText="Music" />
                            <CategoryTab categoryText="Professional Services" />
                            <CategoryTab categoryText="Restaurants" />
                        </ScrollView>
                    </View>

                    <View style={{...styles.section, ...styles.infoSection}}>
                        <View style={styles.infoSectionL}>
                            <Text style={{...styles.infoSectionText, fontSize: wp('4%'), top: hp('1%')}}>RESULTS(5)</Text>
                        </View>
                        <View style={styles.infoSectionR}>
                            <View>
                                <TouchableOpacity 
                                    style={styles.infoSectionRI}
                                    onPress={() => { this.props.navigation.navigate('ListingMap') }}
                                >
                                    <Image 
                                        style={styles.infoSectionRIImage}
                                        source={require('../../assets/listing-page/map.png')} 
                                    />
                                    <Text style={styles.infoSectionText}>Map</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity style={styles.infoSectionRI}>
                                    <Image 
                                        style={styles.infoSectionRIImage}
                                        source={require('../../assets/listing-page/sort.png')} 
                                    />
                                    <Text style={styles.infoSectionText}>Sort</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{...styles.section, ...styles.resultSection}}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                            <ResultCard />
                        </ScrollView>
                    </View>

                    <View style={{height: hp('9%')}}></View>
                </View>
                <FooterTab link={2} />
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
    },
    section: {
        paddingLeft: 16,
        paddingRight: 16
    },
    searchSection: {
        flex: 0.8,
        alignContent: 'center',
        justifyContent: 'center'
    },
    searchSectionL: {
        flexDirection: 'row'
    },
    searchSectionLV: {
        flex: 1
    },
    searchInput: {
        borderColor: '#ffffff5f',
        borderWidth: 2,
        borderRadius: 10,
        margin: 5,
        padding: wp('2%'),
        paddingLeft: wp('9%'),
        height: hp('7.5%'),
        color: '#ffffff'
    },
    searchInputIcon: {
        /* width: wp('4%'),
        height: hp('2%'), */
        width: 15,
        height: 15,
        position: 'absolute',
        top: hp('3.5%'),
        left: wp('4.5%'),
        
    },
    tabSection: {
        flex: 0.3,
        backgroundColor: '#5d3b73'
    },
    infoSection: {
        flex: 0.3,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 5
    },
    infoSectionL: {
        flex: 2
    },
    infoSectionR: {
        flexDirection: 'row'
    },
    infoSectionRI: {
        flex: 1,
        padding: wp('5%'),
        marginLeft: 15,
        alignSelf: 'center',
    },
    infoSectionText: {
        color: '#ffffff',
        fontSize: wp('3.5%'),
        
    },
    infoSectionRIImage: {
        width: wp('3.7%'),
        height: hp('3%'),
        position: 'absolute',
        top: 20    
    },
    resultSection: {
        flex: 2
    },
});

export default Listing;