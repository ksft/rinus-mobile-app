import React from 'react';
import { Button, Image, Text, View, StyleSheet, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class Confirmed extends React.Component {
    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <View style={styles.navView}>
                        <TouchableOpacity 
                            style={styles.navViewButton}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Image
                                style={styles.navViewImage}
                                source={require('../../assets/misc/back-arrow.png')}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{...styles.infoView, justifyContent: 'center'}}>
                        
                        <View style={{height: hp('50%')}}>
                            <View style={styles.imageView}>
                                <View style={{width: wp('20%'), height: wp('20%'), marginBottom: 20}}>
                                    <Image 
                                        style={{flex: 1, width: undefined, height: undefined}}
                                        source={require('../../assets/misc/confirm-icon.png')}
                                    />
                                </View>
                            </View>

                            <View style={styles.headingView}>
                                <Text style={styles.headingViewText}>Booking Confirmed</Text>
                            </View>
                            
                            <View style={styles.detailView}>
                                <View style={styles.detailViewD}>
                                    <View style={styles.detailViewDV}>
                                        <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold', color: '#ffffff'}}>D &amp; D Unisex</Text>                                        
                                        <Text style={{fontSize: wp('3.5%'), color: '#bda1d0'}}>Haircut</Text>
                                    </View>
                                    <View style={styles.detailViewDV}>
                                        <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold', color: '#e8d3f7'}}>03-Sept-2019</Text>                                        
                                        <Text style={{fontSize: wp('3.5%'), color: '#bda1d0'}}>10.30 AM - 11-10 AM</Text>
                                    </View>
                                </View>
                            </View>
                            
                            <View style={styles.optionView}>
                                <TouchableOpacity style={styles.optionView}>
                                    <Image 
                                        style={{width: wp('7%'), height: hp('4%')}}
                                        source={require('../../assets/misc/round-plus.png')} />
                                    <Text style={{color: '#d46eac', fontSize: wp('4.5%')}}> ADD REMINDER </Text>
                                </TouchableOpacity>
                            </View>
                            
                            <View style={styles.noteView}>
                                <Text style={{color: '#9877a0'}}>
                                    You will be reminded one day and one hour before the appointment.
                                </Text>
                                <Text style={{color: '#9877a0'}}>
                                    You can only reschedule the appointment days before date
                                </Text>
                            </View>
                        </View>

                    </View>

                    
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    
    navView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    navViewButton: {
        width: wp('12%'),
        height: hp('7.7%'),
        padding: 10
    },
    navViewImage: {
        flex: 1,
        width: undefined,
        height: undefined
    },

    infoView: {
        flex: 6
    },
    imageView:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headingView:{
        flex: 1,
    },
    headingViewText: {
        color: '#ffffff',
        fontSize: wp('9%'),
        alignSelf: 'center'
    },
    detailView:{
        flex: 1,
        justifyContent: 'center'

    },
    detailViewD: {
        flexDirection: 'row',
        justifyContent: 'center',
        borderWidth: 3,
        borderColor: '#bda1d0',
        borderRadius: 5,
        padding: 15
    },
    detailViewDV: {
        flex: 1,
        justifyContent: 'center'
    },

    optionView:{
        flex: 1,
        flexDirection: 'row',
        paddingTop: 20
    },
    noteView:{
        flex: 1
    },
});

export default Confirmed;