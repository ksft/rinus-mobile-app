import React from 'react';
import { Button, Image, Text, View, StyleSheet, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class Signup2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            emailAddress: ''
        };
    }

    componentDidMount() {
        let { emailAddress } = this.state;
        let receivedEmailAddress = this.props.navigation.getParam('emailAddress', '');
        this.setState({
            emailAddress: receivedEmailAddress
        });
    }

    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <View style={styles.navView}>
                        <TouchableOpacity 
                            style={styles.navViewButton}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Image
                                style={styles.navViewImage}
                                source={require('../../assets/misc/back-arrow.png')}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.headingView}>
                        <Text style={styles.headingViewText}>Contact Details </Text>
                    </View>

                    <View style={styles.formView}>
                        {/* Email */}
                        <View>
                            <Image
                                style={styles.searchInputIcon}
                                source={require("../../assets/misc/envelope.png")}
                            />
                            <TextInput
                                style={styles.searchInput}
                                placeholder="EMAIL ADDRESS"
                                placeholderTextColor="#ffffffa0"
                                value={this.state.emailAddress}
                                onChangeText={(emailAddress) => { this.setState({emailAddress}) }}
                            />
                        </View>
                        {/* Name */}
                        <View>
                            <Image
                                style={{...styles.searchInputIcon, height: hp('4.5%'), width: wp('6.5%')}}
                                source={require("../../assets/misc/user.png")}
                            />
                            <TextInput
                                style={styles.searchInput}
                                placeholder="NAME"
                                placeholderTextColor="#ffffffa0"
                            />
                        </View>
                        {/* Phone */}
                        <View>
                            <Image
                                style={{...styles.searchInputIcon, height: hp('5%'), width: wp('5.7%'), top:5}}
                                source={require("../../assets/misc/phone.png")}
                            />
                            <TextInput
                                style={styles.searchInput}
                                placeholder="PHONE"
                                placeholderTextColor="#ffffffa0"
                            />
                        </View>
                        {/* Password */}
                        <View>
                            <Image
                                style={{...styles.searchInputIcon, height: hp('4.5%'), width: wp('6%')}}
                                source={require("../../assets/misc/lock.png")}
                            />
                            <TextInput
                                style={styles.searchInput}
                                placeholder="SET PASSWORD"
                                placeholderTextColor="#ffffffa0"
                                secureTextEntry={true}
                            />
                        </View>
                    </View>

                    <View style={styles.linkView}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{marginTop: 10, marginBottom: 10, color: '#bda1d0', fontSize: wp('3%'), alignSelf: 'center'}}>
                            By clicking below you agree to </Text>
                            <TouchableOpacity><Text style={{color: '#b85692'}}>Rinus Terms &amp; Conditions</Text></TouchableOpacity>
                        </View>
                        
                        <TouchableOpacity 
                            style={styles.linkViewContinue}
                            onPress={() => this.props.navigation.navigate('Confirmation')}
                        >
                            <Text style={styles.linkViewContinueText}>CREATE AN ACCOUNT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    
    navView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    navViewButton: {
        width: wp('12%'),
        height: hp('7.7%'),
        padding: 10
    },
    navViewImage: {
        flex: 1,
        width: undefined,
        height: undefined
    },
    
    headingView: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        justifyContent: 'flex-start'
    },
    headingViewText: {
        color: '#ffffff',
        fontSize: wp('9%')
    },
    
    formView: {
        flex: 2,
        paddingLeft: 10,
        justifyContent: 'flex-start'
    },
    searchInputIcon: {
        position: 'absolute',
        top: 13,
        left: 0,
        width: wp('7%'),
        height: hp('3%')
    },
    searchInput: {
        paddingLeft: 45,
        borderBottomColor: '#855fa0',
        borderBottomWidth: 2,
        marginBottom: 10,
        color: '#ffffff'
    },
    
    linkView: {
        flex: 1.5,
        padding: 10
    },
    linkViewContinue: {
        padding: 12,
        backgroundColor: '#b85692',
        borderRadius: 5,
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    }, 
    linkViewContinueText: {
        color: '#ffffff',
        alignSelf: 'center',
        // fontWeight: 'bold',
        fontSize: wp('5%')
    },
    linkViewFB: {
        padding: 12,
        backgroundColor: '#2775b7',
        borderRadius: 5,
        justifyContent: 'center',
        flexDirection: 'row',
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    linkViewFBIcon: {
        height: hp('3%'),
        width: wp('4%'),
        marginRight: 20,
        top: 5
    } 
});

export default Signup2;