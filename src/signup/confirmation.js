import React from 'react';
import { Button, Image, Text, View, StyleSheet, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class Confirmation extends React.Component {
    render() {
        return (
            <React.Fragment>
                <View style={styles.screen}>
                    <View style={styles.navView}>
                        <TouchableOpacity 
                            style={styles.navViewButton}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Image
                                style={styles.navViewImage}
                                source={require('../../assets/misc/back-arrow.png')}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.headingView}>
                        <Text style={styles.headingViewText}>Confirmation </Text>
                    </View>

                    <View style={styles.headingView}>
                        <Text style={{color: '#9877a0'}}>Please enter the 4-digit code you received via text message (it should automatically appear below). </Text>
                    </View>

                    <View style={styles.formView}>
                        <View style={styles.formViewCode}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.searchInput}
                                placeholder="-"
                                placeholderTextColor="#ffffffa0"
                                maxLength={1}
                                ref={input => {this.input1 = input}}
                                onSubmitEditing={() => { this.input2.focus() }}
                                blurOnSubmit={false}
                                onChangeText= {text => {this.input2.focus()}}
                            />
                        </View>
                        <View style={styles.formViewCode}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.searchInput}
                                placeholder="-"
                                placeholderTextColor="#ffffffa0"
                                maxLength={1}
                                ref={input => {this.input2 = input}}
                                onSubmitEditing={() => { this.input3.focus() }}
                                blurOnSubmit={false}
                                onChangeText= {text => {this.input3.focus()}}
                            />
                        </View>
                        <View style={styles.formViewCode}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.searchInput}
                                placeholder="-"
                                placeholderTextColor="#ffffffa0"
                                maxLength={1}
                                ref={input => {this.input3 = input}}
                                onSubmitEditing={() => { this.input4.focus() }}
                                blurOnSubmit={false}
                                onChangeText= {text => {this.input4.focus()}}
                            />
                        </View>
                        <View style={styles.formViewCode}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.searchInput}
                                placeholder="-"
                                placeholderTextColor="#ffffffa0"
                                maxLength={1}
                                ref={input => {this.input4 = input}}
                            />
                        </View>
                    </View>

                    <View style={styles.optionView}>
                        <View style={styles.optionViewCode}>
                            <Text style={styles.optionViewCodeText}>RESEND</Text>
                        </View>
                        <View style={styles.optionViewCode}>
                            <Text style={styles.optionViewCodeText}>CHANGE PHONE NUMBER</Text>
                        </View>
                    </View>

                    <View style={styles.linkView}>
                        <TouchableOpacity 
                            style={styles.linkViewContinue}
                            onPress={() => this.props.navigation.navigate('Confirmed')}
                        >
                            <Text style={styles.linkViewContinueText}>DONE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#724d8b',
        padding: 16
    },
    
    navView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    navViewButton: {
        width: wp('12%'),
        height: hp('7.7%'),
        padding: 10
    },
    navViewImage: {
        flex: 1,
        width: undefined,
        height: undefined
    },
    
    headingView: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        justifyContent: 'flex-start'
    },
    headingViewText: {
        color: '#ffffff',
        fontSize: wp('9%')
    },
    
    formView: {
        flex: 2,
        paddingLeft: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    searchInputIcon: {
        position: 'absolute',
        top: 13,
        left: 0,
        width: wp('7%'),
        height: hp('3%')
    },
    formViewCode: {
        flex: 1,
        padding: 5
    },
    searchInput: {
        borderColor: '#9877af',
        borderWidth: 2,
        textAlign: 'center',
        borderRadius: 10,
        paddingTop: 30,
        paddingBottom: 30,
        fontSize: wp('10%'),
        color: '#ffffff'
    },
    
    linkView: {
        flex: 1,
        padding: 10,
        justifyContent: 'flex-start'
    },
    linkViewContinue: {
        padding: 12,
        backgroundColor: '#b85692',
        borderRadius: 5,
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    }, 
    linkViewContinueText: {
        color: '#ffffff',
        alignSelf: 'center',
        // fontWeight: 'bold',
        fontSize: wp('5%')
    },
    linkViewFB: {
        padding: 12,
        backgroundColor: '#2775b7',
        borderRadius: 5,
        justifyContent: 'center',
        flexDirection: 'row',
        
        /* Test Box Shadow */
        borderWidth: 1,
        borderColor: '#00000014',
        borderBottomWidth: 0,
        shadowColor: '#00000014',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    linkViewFBIcon: {
        height: hp('3%'),
        width: wp('4%'),
        marginRight: 20,
        top: 5
    } ,

    optionView: {
        flexDirection: 'row',
    },
    optionViewCode: {
        flex: 1
    },
    optionViewCodeText: {
        color: '#d46eac',
    }
});

export default Confirmation;