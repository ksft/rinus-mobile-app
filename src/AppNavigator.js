import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// Pages Import 
import Signup from './signup/signup';
import Signup2 from './signup/signup2';
import Confirmation from './signup/confirmation';
import Confirmed from './signup/confirmed';
import Home from './home/home';
import Listing from './listing/listing';
import ListingMap from './listing/listing-map';
import Profile from './profile/profile';
import Details from './details/details';
import BookingSelect from './details/booking-select';
import Appointments from './appointments/appointments';

const AppNavigator = createStackNavigator({
    Signup: {
      screen: Signup
    },
    Signup2: {
      screen: Signup2
    },
    Confirmation: {
      screen: Confirmation
    },
    Confirmed: {
      screen: Confirmed
    },
    Home: {
        screen: Home
    },
    Listing: {
        screen: Listing
    },
    Profile: {
      screen: Profile
    },
    ListingMap: {
      screen: ListingMap
    },
    Details: {
      screen: Details
    },
    BookingSelect: {
      screen: BookingSelect
    }, 
    Appointments: {
      screen: Appointments
    }
},{
  initialRouteName: 'Home',
  /* The header config from HomeScreen is now here */
  defaultNavigationOptions: {
    headerStyle: {
      display: 'none',
    }
  },
});

export default createAppContainer(AppNavigator);